#!/bin/bash

# TODO: convert this draft to full self-testing plugin (koha docker) and autoversioning.

VERSION=0.1

RELEASE_FILE="koha-plugin-database-check-${VERSION}.kpz"

echo "Building release package ${RELEASE_FILE}"

if [ -e ${RELEASE_FILE} ]
then
  rm ${RELEASE_FILE}
fi

mkdir dist
cp -r Koha dist/.
cd dist
zip -r ../${RELEASE_FILE} ./Koha
cd ..
rm -rf dist
