package Koha::Plugin::Com::ProperPerl::CheckDBPackOne;

use Modern::Perl;
use base qw(Koha::Plugins::Base);

use CGI;
use C4::Context;
use C4::Auth;
use Koha::Patron;

our $VERSION         = "0.1";
our $MINIMUM_VERSION = "0.1";

our $metadata = {
    name            => 'Database Check',
    author          => 'Petro Nugged',
    date_authored   => '2020-12-02',
    date_updated    => "2021-01-14",
    minimum_version => $MINIMUM_VERSION,
    maximum_version => undef,
    version         => $VERSION,
    description     => 'This is a frame for repots generation and Database checks plugin.',
};

sub new {
    my ( $class, $args ) = @_;

    $args->{'metadata'} = $metadata;
    $args->{'metadata'}->{'class'} = $class;

    my $self = $class->SUPER::new($args);
    return $self;
}

sub tool {
    my ( $self, $args ) = @_;
    my $cgi = $self->{cgi};

    my $template = $self->get_template( { file => 'tool.tt' } );

    my @patrons_list = Koha::Patrons->search({
            dateofbirth => [
                 undef,
                 '0000-00-00',
                 { '<=' => '1900-01-01' },
                 { '>=' => \'NOW()' },
        ],
    }, { rows => 100 } )->as_list;
    # just for example, but if big DB we need part, ->count(), pagination, all doable
    # this for example, later we might go with pagination and more filters + dropdown lists
    # and parameters. Also possible to make some more complicated features.

    $template->param( 'patrons_list'  => \@patrons_list );
    $template->param( 'patrons_total' => scalar @patrons_list );

    my $t = $cgi ? join ", ", $cgi->param : '-undef-';
    $template->param( 'debug' => "CGI: $t" );

    $self->output_html( $template->output() );
}

sub intranet_catalog_biblio_enhancements {
    my ( $self, $args ) = @_;

    return $self->retrieve_data('intranet_catalog_biblio_enhancements') eq 'Yes';
}

sub intranet_catalog_biblio_enhancements_toolbar_button {
    my ($self) = @_;
    my $cgi = $self->{cgi};


    if( ! $cgi ) {
        $cgi = CGI->new();
        # hack to get CGI: for some reason it is not allways presented
        # in all *.pls which have "catalog_biblio_enhancements_toolbar_button"
    }
    return 'NO CGI!' if !$cgi;

    return q|
        <div class="btn-group">
            <a id="some_biblioitem_related_features_example_button"
                class="btn btn-default btn-sm"
                href="/cgi-bin/koha/plugins/run.pl?class=|.
                ref($self)
                .q|&method=some_biblioitem_related_features_example&biblionumber=|.
                scalar $cgi->param('biblionumber')
                .q|">
                FEATURE EXAMPLE
            </a>
        </div>
    |;
}

sub intranet_js {
    my ($self) = @_;

    state $ncpu = `nproc`;
    # todo: yes, this is annoying to show on each pageload :).
    my $LA_15 = (split(/ /,`cat /proc/loadavg`))[2] || 0 / $ncpu;

    my $alert = $LA_15 < 1 ? '' : 'window.alert("Hey! current server LA is high! Do something!");';

    return qq|
        <script>
            $LA_15 
            console.log("Current server LA is $LA_15. " +
                "Just demo, but we can inject even some more complicated intefracing things");
        </script>
    |;
}

sub some_biblioitem_related_features_example {
    my ( $self, $args ) = @_;
    my $cgi = $self->{cgi};

    my $template = $self->get_template( { file => 'some_biblioitem_related_features_example.tt' } );

    $template->param( biblionumber => scalar $cgi->param('biblionumber') );

    my $t = $cgi ? join ", ", $cgi->param : '-undef-';
    $template->param( 'debug' => "CGI: $t" );

    $self->output_html( $template->output() );
}

sub install() {
    return 1;
}

sub upgrade {
    return 1;
}

sub uninstall() {
    return 1;
}

1;
